--[[
	RecipeKnown Tooltips File
	v<%version%>
	
	Revision: $Id: Tooltips.lua 22 2014-12-16 14:34:02 PST Kjasi $
]]

local rk = _G.RecipeKnown
if (not rk) then
	print(RED_FONT_COLOR_CODE.."Tooltips are unable to find RecipeKnown's Global."..FONT_COLOR_CODE_CLOSE)
	return
end
local L = rk.Localization
if (not L) then
	print(RED_FONT_COLOR_CODE.."Tooltips are unable to find RecipeKnown's Localization data."..FONT_COLOR_CODE_CLOSE)
	return
end
local C = rk.Constants
if (not C) then
	print(RED_FONT_COLOR_CODE.."Tooltips are unable to find RecipeKnown's Constants data."..FONT_COLOR_CODE_CLOSE)
	return
end
local DB = rk.Database
if (not DB) then
	print(RED_FONT_COLOR_CODE.."Tooltips are unable to find RecipeKnown's Database data."..FONT_COLOR_CODE_CLOSE)
	return
end

rk.ToolTip = {}
local Tt = rk.ToolTip

local tooltip = RecipeKnown_Tooltip_Game
local lastlink = nil

local LibKJ = LibStub("LibKjasi-1.0")

local Color = {
	["White"] = "|cffffffff",
	["Red"] = "|cffff0000",
	["Green"] = "|cff11ff11",
}

-- Set & Show Tooltip
function RecipeKnown_Tooltip_Show(self)
	local Anch, AnchP

	tooltip:SetScale(UIParent:GetEffectiveScale()-0.10)

	-- MUST come after full generation of the tooltip, else GetWidth won't return the proper size.
	local ttipr, rttipw, uir = self:GetRight(), tooltip:GetWidth(), UIParent:GetRight()
	if ttipr == nil then
		ttipr = 200
	end
	if rttipw == nil then
		rttipw = 200
	end
	if uir == nil then
		uir = 1024
	end

	if (ttipr+rttipw > uir) then
		Anch = "TOPRIGHT"
		AnchP = "TOPLEFT"
	else
		Anch = "TOPLEFT"
		AnchP = "TOPRIGHT"
	end
		
	tooltip:SetPoint(Anch, self, AnchP)
	tooltip:Show()
end

-- Hide our Tooltip
function RecipeKnown_Tooltip_Hide(self)
	--== WoW Tooltips ==--
	if (self:GetName() == "ItemRefTooltip") or (self:GetName() == "RecipeKnown_Tooltip_Item") then
		RecipeKnown_Tooltip_Item:ClearLines()
		RecipeKnown_Tooltip_Item:Hide()
	elseif (self:GetName() == "GameTooltip") or (self:GetName() == "RecipeKnown_Tooltip_Game") then
		RecipeKnown_Tooltip_Game:ClearLines()
		RecipeKnown_Tooltip_Game:Hide()

	--== For Addons ==--
	-- AtlasLoot
	elseif (self:GetName() == "AtlasLootTooltipTEMP") then
		RecipeKnown_Tooltip_Item:ClearLines()
		RecipeKnown_Tooltip_Item:Hide()

	-- If we can't find what we're hiding, then clear & hide everything.
	else
		RecipeKnown_Tooltip_Item:ClearLines()
		RecipeKnown_Tooltip_Item:Hide()
		RecipeKnown_Tooltip_Game:ClearLines()
		RecipeKnown_Tooltip_Game:Hide()
	end
end

-- Load and Process the Tooltip
--[[
function RecipeKnown_Tooltip_Load(self)
	local name, link = self:GetItem()
	if (not name) then
		return
	end
	
	if (not self:IsVisible()) then
		--rk:Msg("Parent Tooltip is hidden!")
		Tt:Hide(self)
		return
	end
	
	local isPet = Tt:Scan(link,L["Tooltip Scan Teaches Pet"])
	
end
]]

local function getTooltipType(self)
	if self:GetName() == "GameTooltip" then
		tooltip = RecipeKnown_Tooltip_Game
	else
		tooltip = RecipeKnown_Tooltip_Item
	end
end









-- Tooltip Hooks
function RecipeKnown_Tooltip_Load(self)
	local name, link = self:GetItem()
	if (not name) then
		return
	end
	
	getTooltipType(self)
	
	--rk:Msg("Tooltip Show")
	local returnfail = false

	local itemname,_,_,_,_,itemtype,itemsubtype = GetItemInfo(link)
	local isPet = Tt:Scan(link,L["Tooltip Scan Teaches Pet"])
	if ((itemtype == L["Itemtype Misc"]) and (itemsubtype == L["Itemsubtype Pet"])) then
		isPet = true
	end
	if (isPet) then
		itemsubtype = L["Itemsubtype Pet"]
	end

	if (not itemname) or ((itemtype ~= L["Itemtype Recipe"]) and ((itemtype ~= L["Itemtype Misc"]) or ((itemsubtype ~= L["Itemsubtype Lockbox"]) and (itemsubtype ~= L["Itemsubtype Pet"])))) then
		returnfail = true
	end

	if (returnfail == true) then
		--rk:Msg("Itemtype wrong. Hiding Tooltip.")
		RecipeKnown_Tooltip_Hide(self)
		return
	end

	if (tooltip:IsVisible()) and (link == lastlink) then
		--rk:Msg("Tooltip Visible, or link is current link.")
		return
	end

	--rk:Msg("Starting Prescan...")
	RecipeKnown_Tooltip_PreScan(self, link)
	lastlink = link
end

function RecipeKnown_Tooltip_SetHyperlink(self, link)
	if (not link) then
		--rk:Msg("No Hyperlink")
		return
	end

	--rk:Msg("Set Hyperlink")

	if (not self:IsVisible()) then
		--rk:Msg("Parent Tooltip is hidden!")
		RecipeKnown_Tooltip_Hide(self)
		return
	end

	getTooltipType(self)

	if (tooltip:IsVisible()) then
		return
	end

	local _, type = rk:GetItemID(link)

	if (type ~= "item") and (type ~= "enchant") then
		if (tooltip:IsVisible()) then
			--rk:Msg("Tooltip Found. Hiding...")
			RecipeKnown_Tooltip_Hide(self)
		end
		--rk:Msg("Itemtype wrong. Stopping...")
		return
	end

	--rk:Msg("Setting Hyperlink \""..tostring(link).."\"...")

	if (not tooltip:IsVisible()) or (link ~= lastlink) then
		RecipeKnown_Tooltip_PreScan(self, link)
		lastlink = link
	end
end

-- Tooltip Pre-Scan Filter
function RecipeKnown_Tooltip_PreScan(self, link)
	--rk:Msg("Running Prescan...")
	if (not link) then
		rk:Msg("Cancelling Prescan: No Link")
		return
	end
	
	getTooltipType(self)

	local id, type = rk:GetItemID(link)

	local baditem = false
	if (type == "item") then
		for k,badid in pairs(DB.BadItems) do
			--rk:Msg("BadID:"..tostring(badid)..", ID:"..id)
			if (tonumber(badid) == tonumber(id)) then
				--rk:Msg("Badid found:"..tostring(badid))
				baditem = true
				break
			end
		end
	end

	--rk:Msg("Item Type: "..tostring(type))

	if (baditem == true) then
		--rk:Msg("BadItem Found")
		tooltip:Hide()
		return
	elseif (type == "item") then
		--rk:Msg("Type = Item")
		local itemname,_,_,_,_,itemtype,itemsubtype = GetItemInfo(link)
		local isPet = Tt:Scan(link,L["Tooltip Scan Teaches Pet"])
		if ((itemtype == L["Itemtype Misc"]) and (itemsubtype == L["Itemsubtype Pet"])) then
			isPet = true
		end
		if (isPet) then
			itemsubtype = L["Itemsubtype Pet"]
		end
		if (not itemname) or ((itemtype ~= L["Itemtype Recipe"]) and ((itemtype ~= L["Itemtype Misc"]) or ((itemsubtype ~= L["Itemsubtype Lockbox"]) and (itemsubtype ~= L["Itemsubtype Pet"])))) then
			if (tooltip:IsVisible()) then
				--rk:Msg("Bad Name, or isn't a supported item")
				RecipeKnown_Tooltip_Hide(tooltip)
			end
			return
		end
		RecipeKnown_ItemTooltip(self, itemname, link)
		return
	elseif (type == "enchant") then
		--rk:Msg("Type = Enchant")
		local itemname,_,_,_,_,itemtype,_ = GetItemInfo(link)
		if (not itemname) or (itemtype ~= L["Itemtype Recipe"]) then
			if (tooltip:IsVisible()) then
				RecipeKnown_Tooltip_Hide(tooltip)
			end
			return
		end
		RecipeKnown_EnchantTooltip(self, itemname, link)
		return
	end
	--rk:Msg("Type ~= Item")
end

--== Main Tooltip Functions ==--
--Filter Item data for tooltip
function RecipeKnown_ItemTooltip(self, name, ilink)
	-- rk:Msg("Running Tooltip")
	if (not iname) or (iname == nil) or (not ilink) or (ilink == nil) then
		iname, ilink = self:GetItem()
	end
	RecipeKnown_Tooltip(self, "item", iname, ilink)
end

--Filter Enchant data for tooltip
function RecipeKnown_EnchantTooltip(self, name, link)
	RecipeKnown_Tooltip(self, "enchant", name, link)
end

function RecipeKnown_Tooltip(self, type, name, link)
	--rk:Msg("Running RK Tooltip...")
	
	getTooltipType(self)
	
	--rk:Msg("Setting locals")

	tooltip:SetOwner(self,"ANCHOR_NONE")
	tooltip:ClearLines()
	tooltip:AddLine(Color["White"]..RECIPEKNOWN_TOOLTIP_REPORT.."\n", 1,1,1)

	local Text = Color["White"]..RECIPEKNOWN_TOOLTIP_REPORT.."\n"

	if (type == "item") then
		local info, job, reqskill, ispet, kcount, ckcount, dkcount = RecipeKnown_Tooltip_GetInfo(link)
		--print("Info Count: "..LibKJ:tcount(info))
		if (not info) or (info == nil) or (LibKJ:tcount(info) == 0) then
			--rk:Msg("No Info or Info == nil")
			return
		end

		local known, canknow, dontknow = {}, {}, {}
		local color, nameonly, isLockbox
		local count, wkcount = 0, 0

		--rk:Msg("Building Arrays")
		-- Build the arrays
		for toon,data in pairs(info) do
			--rk:Msg("Data[Min]:"..tostring(data["min"])..", Data[Max]:"..tostring(data["max"])..", Data[Known]:"..tostring(data["KnowsThis"]))	

			if (data["KnowsThis"] == true) then
				known[toon] = 1
				--kcount = kcount + 1
			elseif (not ispet) and (tonumber(data["min"]) >= tonumber(reqskill)) then
				canknow[toon] = 1
				--ckcount = ckcount + 1
			else
				dontknow[toon] = 1
				--dkcount = dkcount + 1
			end
			count = count + 1
		end

		--rk:Msg("Local Count: "..tostring(count)..", K:"..tostring(kcount)..", C:"..tostring(ckcount)..", D:"..tostring(dkcount))

		if (job == L["Skill Lockpicking"]) then
			isLockbox = true
		end
		
		--rk:Msg("Building Message")
		-- Build the actual Message
		if (kcount > 0) then
			sort(known)
			if (ispet == true) then
				tooltip:AddLine(Color["White"]..format(L["Pet Is Available"],kcount,count))
				nameonly = true
			elseif (isLockbox == true) then
				tooltip:AddLine(Color["White"]..format(RECIPEKNOWN_TOOLTIP_CANLOCKPICK,kcount,count))
			else
				tooltip:AddLine(Color["White"]..format(RECIPEKNOWN_TOOLTIP_ALREADYKNOWN,kcount,count))
			end
			for toon,v in pairs(known) do
				if (nameonly == true) then
					tooltip:AddLine(Color["Green"]..format(L["Tooltip Toon Only"],info[toon]["name"]))
				elseif (isLockbox == true) then
					tooltip:AddLine(Color["Green"]..format(L["Tooltip Toon Profession"],info[toon]["name"],tostring(info[toon]["job"]),info[toon]["min"]))
				else
					tooltip:AddLine(Color["Green"]..format(L["Tooltip Toon Skill"],info[toon]["name"],info[toon]["min"],info[toon]["max"]))
				end
			end
		end

		-- Currently only active for recipes
		if (ckcount > 0) then
			sort(canknow)
			local st = ""
			if (kcount > 0) then st = "\n" end
			if (isLockbox == true) then
				tooltip:AddLine(st..Color["White"]..format(RECIPEKNOWN_TOOLTIP_COULDLOCKPICK,ckcount,count))
			else
				tooltip:AddLine(st..Color["White"]..format(RECIPEKNOWN_TOOLTIP_CANKNOW,ckcount,count))
			end
			for toon,v in pairs(canknow) do
				if (nameonly == true) then
					tooltip:AddLine(Color["White"]..format(L["Tooltip Toon Only"],info[toon]["name"]))
				elseif (isLockbox == true) then
					tooltip:AddLine(Color["White"]..format(L["Tooltip Toon Profession"],info[toon]["name"],tostring(info[toon]["job"]),info[toon]["min"]))
				else
					tooltip:AddLine(Color["White"]..format(L["Tooltip Toon Skill"],info[toon]["name"],info[toon]["min"],info[toon]["max"]))
				end
			end
		end

		if (dkcount > 0) then
			sort(dontknow)
			local st = ""
			if (kcount > 0) or (ckcount > 0) then st = "\n" end
			if (ispet == true) then
				tooltip:AddLine(st..Color["White"]..format(L["Pet Not Available"],dkcount,count))
				nameonly = true
			elseif (isLockbox == true) then
				tooltip:AddLine(st..Color["White"]..format(RECIPEKNOWN_TOOLTIP_CANNOTLOCKPICK,dkcount,count))
			else
				tooltip:AddLine(st..Color["White"]..format(RECIPEKNOWN_TOOLTIP_DONTKNOW,dkcount,count))
			end
			for toon,v in pairs(dontknow) do
				if (nameonly == true) then
					tooltip:AddLine(Color["Red"]..format(L["Tooltip Toon Only"],info[toon]["name"]))
				elseif (isLockbox == true) then
					tooltip:AddLine(Color["Red"]..format(L["Tooltip Toon Profession"],info[toon]["name"],tostring(info[toon]["job"]),info[toon]["min"]))
				else
					tooltip:AddLine(Color["Red"]..format(L["Tooltip Toon Skill"],info[toon]["name"],info[toon]["min"],info[toon]["max"]))
				end
			end
		end

		-- Build No Data Info
		if (kcount <= 0) and (ckcount <= 0) and (dkcount <= 0) then
			rk:Msg("Count Fail: "..kcount..", "..ckcount..", "..dkcount)
			return
		end
	elseif (type == "enchant") then
		return
	end

	RecipeKnown_Tooltip_Show(self)
end

function RecipeKnown_Tooltip_GetInfo(link)
	if (not link) or (link == nil) then
		rk:Msg("RecipeKnown_Tooltip_GetInfo Link Fail")
		return
	end
	--rk:Msg("Getting Info")

	local isapet = nil
	local itemid = rk:GetItemID(link)
	local itemname,_,_,_,_,itemtype,itemsubtype = GetItemInfo(link)
	local kcount, ckcount, dkcount = 0,0,0

	if (not itemname) or ((itemtype ~= L["Itemtype Recipe"]) and ((itemtype ~= L["Itemtype Misc"]) or ((itemsubtype ~= L["Itemsubtype Lockbox"]) and (itemsubtype ~= L["Itemsubtype Pet"])))) then
		rk:Msg("Unsupported Item Type")
		return
	end

	local tsid = rk:Link2TsID(link)
	local job, reqskill = RecipeKnown_Tooltip_GetJob(link)
	local data = {}

	--[[
	if job and reqskill then
		rk:Msg("Testing Recipe for "..tostring(job).." with reqskill of "..tostring(reqskill))
	end
	]]

	-- Lockboxes
	if ((itemsubtype == L["Itemsubtype Lockbox"]) and (job == L["Skill Lockpicking"])) then
		-- Rogues
		for toon, v in pairs(RecipeKnown_CharData[rk.Realm][rk.Faction]["data"]) do
			if (v["Class"] == "ROGUE") then
				local LPLevel = tonumber(v["Level"]) * 5
				if (not data[toon.."Lockpick"]) then
					data[toon.."Lockpick"] = {
						["name"] = toon,
						["min"] = LPLevel,
						["max"] = LPLevel,
						["job"] = L["Skill Lockpicking"],
						["KnowsThis"] = false
					}
				end
				if (LPLevel >= tonumber(reqskill)) then
					kcount = kcount + 1
					data[toon.."Lockpick"]["KnowsThis"] = true
				else
					dkcount = dkcount + 1
				end
			end
		end
		
		-- Blacksmiths & Engineers
		for altjob, jv in pairs(RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"]) do
			--rk:Msg("Checking Job: "..tostring(altjob))
			if (altjob == L["Skill Blacksmithing"]) or (altjob == L["Skill Engineering"]) then
				--rk:Msg("Processing Blacksmiths and Engineers...")
				for toon, v in pairs(jv) do
					local jlvl = v["data"]["current"]
					local tjlvl = jlvl
					local skilldb, recipedb
					
					-- Set skill database depending on Job...
					if (altjob == L["Skill Blacksmithing"]) then
						skilldb = C.LockboxSkills_Blacksmith
						recipedb = C.LockboxSkillRecipes_Blacksmith
					elseif (altjob == L["Skill Engineering"]) then
						skilldb = C.LockboxSkills_Engineer
						recipedb = C.LockboxSkillRecipes_Engineer
					end
					
					-- Find a recipe for this skill level, or stop at 1...
					while ((not skilldb[tjlvl]) and (tjlvl ~= 1)) do
						tjlvl = tjlvl - 1
					end
					-- Add data about this toon.
					if (not data[toon..altjob]) then
						data[toon..altjob] = {
							["name"] = toon,
							["min"] = jlvl,
							["max"] = v["data"]["max"],
							["job"] = altjob,
							["KnowsThis"] = false
						}
					end
					--rk:Msg("Scanned Job Level: "..tostring(tjlvl))
					
					-- Process the data and determine if we know a recipe for this box.
					if (tjlvl >= 100) and (skilldb[tjlvl]) and (skilldb[tjlvl] >= tonumber(reqskill)) then
						-- This toon should be able to unlock it. Now we'll scan recipes to see if we know a matching recipe!
						local recipeid = recipedb[tjlvl]
						if (v["recipes"][tostring(recipeid)]) then
							-- This toon knows a recipe to unlock this box!
							kcount = kcount + 1
							data[toon..altjob]["KnowsThis"] = true
						else
							-- This toon can learn a recipe to unlock this box!
							ckcount = ckcount + 1
						end
					else
						dkcount = dkcount + 1
					end
				end
			end
		end
	end

	-- Recipes
	if (itemtype == L["Itemtype Recipe"]) then
		-- Tradeskill Recipes
		if (RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][job]) then
			for toon, v in pairs(RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][job]) do
				local tj = toon..job
				--rk:Msg(tostring(toon).." has "..tostring(job))
				if (not data[tj]) then
					data[tj] = {
						["name"] = toon,
						["min"] = v["data"]["current"],
						["max"] = v["data"]["max"],
						["job"] = job,
						["KnowsThis"] = false
					}
				end

				if (v["recipes"][tsid]) then
					kcount = kcount + 1
					data[tj]["KnowsThis"] = true
				elseif (data[tj]["min"] >= tonumber(reqskill)) then
					ckcount = ckcount + 1
				elseif (data[tj]["min"] < tonumber(reqskill)) then
					dkcount = dkcount + 1
				end
			end
		end
	end

	return data, job, reqskill, isapet, kcount, ckcount, dkcount
end

function Tt:Scan(link, string)
	if (not link) or (link == nil) or (not string) or (string == nil) then
		return
	end

	RecipeKnown_Tooltip_Scanner:ClearLines()
	RecipeKnown_Tooltip_Scanner:SetHyperlink(link)

	for i=1,RecipeKnown_Tooltip_Scanner:NumLines() do
		local text = getglobal("RecipeKnown_Tooltip_ScannerTextLeft"..i):GetText()

		-- Search to see if this has the specified line
		if (strfind(text,string)) then
			return true
		end
	end
	return
end

function RecipeKnown_Tooltip_GetAlreadyKnown(link)
	if (not link) or (link == nil) then
		return
	end

	return Tt:Scan(link, ITEM_SPELL_KNOWN)
end


function RecipeKnown_Tooltip_GetJob(link)
	if (not link) or (link == nil) then
		--rk:Msg("Tooltip Scanner failed: link == nil")
		return
	end
	-- rk:Msg("Tooltip Scanner Running...")

	RecipeKnown_Tooltip_Scanner:ClearLines()
	RecipeKnown_Tooltip_Scanner:SetHyperlink(link)
	
	-- Transform Blizzard's Item Skill Requirement string into a pattern to match
	local ims = ITEM_MIN_SKILL
	ims = gsub(ims, "%(", "%%(")
	ims = gsub(ims, "%)", "%%)")
	ims = gsub(ims, "%%s", "%%a+")
	ims = gsub(ims, "%%d", "%%d+")

	--rk:Msg("Num Lines:"..RecipeKnown_Tooltip_Scanner:NumLines())
	for i=1,RecipeKnown_Tooltip_Scanner:NumLines() do
		--rk:Msg("Scanning Tooltip...")
		local text = getglobal("RecipeKnown_Tooltip_ScannerTextLeft"..i):GetText()
		--rk:Msg("Tooltip Text:"..tostring(text))

		--rk:Msg("String: "..tostring(strfind(text,ims))..", "..tostring(strfind(text,ITEM_MIN_SKILL)))
		-- Search for Tradeskill name and required skill level
		
		if strfind(text, ims) then
			--rk:Msg("String Found!")
			local job, req = strmatch(text,L["Tooltip Scan Skill Requirement"])
			--rk:Msg("Job: \""..tostring(job).."\", req:"..tostring(req))
			if (job) and (job ~= nil) then
				--rk:Msg("Found a recipe for "..tostring(job).." with a Req of "..tostring(req))
				return job, req
			end
		end
	end
end