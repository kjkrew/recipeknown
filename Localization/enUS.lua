--[[
	ReceipeKnown v<%version%>
	English Localization File

	This file only effects the visual aspects of the Addon, not the functionality.
	
	Revision: $Id: enUS.lua 17 2012-12-23 12:13:12 PST Kjasi $
]]

local rk = _G.RecipeKnown
if (not rk) then
	print(RED_FONT_COLOR_CODE.."enUS Localization is unable to find RecipeKnown's Global."..FONT_COLOR_CODE_CLOSE)
	return
end
local L = rk.Localization
if (not L) then
	print(RED_FONT_COLOR_CODE.."enUS Localization is unable to find RecipeKnown's Localization data."..FONT_COLOR_CODE_CLOSE)
	return
end

-- Message Templates
L["Message"] = "|CFF57A5BB%s:"..FONT_COLOR_CODE_CLOSE.." %s"
L["Message Error"] = "|CFFFF2020%s Error:"..FONT_COLOR_CODE_CLOSE.." %s"

-- Basic
L["Error"] = "Error"
L["Version"] = "Version "
L["Is Now Loaded"] = "Version %s is now loaded!"


-- Modes
RECIPEKNOWN_MODE_SIMPLE = "Simple Mode"
RECIPEKNOWN_MODE_SELFISH = "Selfish Mode"
RECIPEKNOWN_MODE_ADVANCED = "Advanced Mode"


-- Error Messages
L["Error No Record"] = "RecipeKnown does not have a record for %s (Itemid: %s). Please give this information to the development team, so they can add it."
L["Error Database Options Deleted"] = "Due to major database changes, your Options has been deleted. We apologize for any inconveniences."
L["Error Database Character Data Deleted"] = "Due to major database changes, your Tradeskill data  has been deleted. We apologize for any inconveniences."

-- Tooltip Text
RECIPEKNOWN_TOOLTIP_REPORT = "RecipeKnown Report"
RECIPEKNOWN_TOOLTIP_ALREADYKNOWN = "This is already known by %d/%d:"			-- Passed Variable: Known/Total
RECIPEKNOWN_TOOLTIP_CANKNOW = "This can be learned by %d/%d:"					-- Passed Variable: Known/Total
RECIPEKNOWN_TOOLTIP_DONTKNOW = "This can not yet be learned by %d/%d:"			-- Passed Variable: Known/Total
RECIPEKNOWN_TOOLTIP_CANLOCKPICK = "This can be opened by %d/%d:"				-- Passed Variable: Known/Total
RECIPEKNOWN_TOOLTIP_COULDLOCKPICK = "This could be opened %d/%d:"				-- Passed Variable: Known/Total
RECIPEKNOWN_TOOLTIP_CANNOTLOCKPICK = "This can not yet be opened by %d/%d:"		-- Passed Variable: Known/Total
L["Pet Is Available"] = "This pet is available to %d/%d:"						-- Passed Variable: Known/Total
L["Pet Not Available"] = "This pet is not available to %d/%d:"					-- Passed Variable: Known/Total