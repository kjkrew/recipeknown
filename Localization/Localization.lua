﻿--[[
	RecipeKnown v<%version%>
	Base Localization

	This file contains primary localizations needed for RecipeKnown to function in the given locale.
	Additonal localization files are for purely graphical changes.

	Revision: $Id: localization.lua 19 2014-12-16 15:10:58 PST Kjasi $
]]

local rk = _G.RecipeKnown
if (not rk) then
	print(RED_FONT_COLOR_CODE.."Localization is unable to find RecipeKnown's Global."..FONT_COLOR_CODE_CLOSE)
	return
end

rk.Localization = {}
local L = rk.Localization
local Locale = GetLocale()

L["RK Title"] = "RecipeKnown"

-- Automatic Localizations
--Item Types & Subtypes
rk.ItemTypes = { GetAuctionItemClasses() }
rk.ItemSubTypes = {}
rk.ItemSubTypes.Recipe = { GetAuctionItemSubClasses(rk.RecipeType) }
rk.ItemSubTypes.Misc = { GetAuctionItemSubClasses(rk.MiscType) }

L["Itemtype Recipe"] = rk.ItemTypes[rk.RecipeType]
L["Itemtype Misc"] = rk.ItemTypes[rk.MiscType]
L["Itemsubtype Lockbox"] = rk.ItemSubTypes.Misc[rk.MiscSubType.Junk]

L["Skill Alchemy"] = rk.ItemSubTypes.Recipe[rk.RecipeSubType.Alchemy]
L["Skill Blacksmithing"] = rk.ItemSubTypes.Recipe[rk.RecipeSubType.Blacksmithing]
L["Skill Enchanting"] = rk.ItemSubTypes.Recipe[rk.RecipeSubType.Enchanting]
L["Skill Engineering"] = rk.ItemSubTypes.Recipe[rk.RecipeSubType.Engineering]
L["Skill Inscription"] = rk.ItemSubTypes.Recipe[rk.RecipeSubType.Inscription]
L["Skill Jewelcrafting"] = rk.ItemSubTypes.Recipe[rk.RecipeSubType.Jewelcrafting]
L["Skill Leatherworking"] = rk.ItemSubTypes.Recipe[rk.RecipeSubType.Leatherworking]
L["Skill Tailoring"] = rk.ItemSubTypes.Recipe[rk.RecipeSubType.Tailoring]

-- These should work for most countries. Only needs to be included if the format changes for that locale.
L["Tooltip Scan Skill Requirement"] = "(%a+) %((%d+)%)"	-- Retrieves data from "Requires Profession (SkillLevel)" strings
L["Tooltip Scan Level Requirement"] = "%a+ %a+ (%d+)"
L["Tooltip Scan Class Requirement"] = "%a+: (%a+)"

-- Listings in the Tooltips
L["Tooltip Toon Only"] = "  %s"							-- Passed Variable:  Character Name
L["Tooltip Toon Skill"] = "  %s (%d/%d)"				-- Passed Variables: Character Name, Current Skill, Maximum Skill
L["Tooltip Toon Profession"] = "  %s (%s, %d)"			-- Passed Variables: Character Name, Profession Name, Current Skill
L["Tooltip Toon Spells"] = "  %s (%d)"					-- Passed Variables: Character Name, Character Level


-- English
-- Defaults to this	
	L["Skill Lockpicking"] = "Lockpicking"
	
	L["Recipe Prefix Design"] = "Design: (%a+)"
	L["Recipe Prefix Formula"] = "Formula: (%a+)"
	L["Recipe Prefix Manual"] = "Manual: (%a+)"
	L["Recipe Prefix Plans"] = "Plans: (%a+)"
	L["Recipe Prefix Pattern"] = "Pattern: (%a+)"
	L["Recipe Prefix Recipe"] = "Recipe: (%a+)"
	L["Recipe Prefix Schematic"] = "Schematic: (%a+)"
	L["Recipe Prefix Technique"] = "Technique: (%a+)"

-- German
if (GetLocale() == "deDE") then
	L["Skill Lockpicking"] = "Schlossknacken"

	L["Recipe Prefix Design"] = "Vorlage: (%a+)"
	L["Recipe Prefix Formula"] = "Formel: (%a+)"
	L["Recipe Prefix Manual"] = "Handbuch: (%a+)"
	L["Recipe Prefix Plans"] = "Pläne: (%a+)"
	L["Recipe Prefix Pattern"] = "Muster: (%a+)"
	L["Recipe Prefix Recipe"] = "Rezept: (%a+)"
	L["Recipe Prefix Schematic"] = "Bauplan: (%a+)"
	L["Recipe Prefix Technique"] = "Technik: (%a+)"

-- French
elseif (GetLocale() == "frFR") then
	L["Skill Lockpicking"] = "Crochetage"

	L["Recipe Prefix Design"] = "Dessin : (%a+)"
	L["Recipe Prefix Formula"] = "Formel : (%a+)"
	L["Recipe Prefix Manual"] = "Manuel : (%a+)"
	L["Recipe Prefix Plans"] = "Plans : (%a+)"
	L["Recipe Prefix Pattern"] = "Patron : (%a+)"
	L["Recipe Prefix Recipe"] = "Recette : (%a+)"
	L["Recipe Prefix Schematic"] = "Schéma : (%a+)"
	L["Recipe Prefix Technique"] = "Technique : (%a+)"

-- Spanish
elseif (GetLocale() == "esES") or (GetLocale() == "esMX") then
	L["Skill Lockpicking"] = "Forzar cerraduras"

	L["Recipe Prefix Design"] = "Boceto: (%a+)"
	L["Recipe Prefix Formula"] = "Fórmula: (%a+)"
	L["Recipe Prefix Manual"] = "Manual: (%a+)"
	L["Recipe Prefix Plans"] = "Diseño: (%a+)"
	L["Recipe Prefix Pattern"] = "Patrón: (%a+)"
	L["Recipe Prefix Recipe"] = "Receta: (%a+)"
	L["Recipe Prefix Schematic"] = "Esquema: (%a+)"
	L["Recipe Prefix Technique"] = "Técnica: (%a+)"

-- Simple Chinese
elseif (GetLocale() == "zhCN") then
	L["Skill Lockpicking"] = "开锁"

	L["Recipe Prefix Design"] = "图鉴：(%a+)"
	L["Recipe Prefix Formula"] = "公式：(%a+)"
	L["Recipe Prefix Formula 2"] = "配方：(%a+)"	-- Alchemy
	L["Recipe Prefix Manual"] = "手册：(%a+)"
	L["Recipe Prefix Pattern"] = "图样：(%a+)"
	L["Recipe Prefix Plans"] = "设计图：(%a+)"
	L["Recipe Prefix Recipe"] = "食谱：(%a+)"
	L["Recipe Prefix Schematic"] = "结构图：(%a+)"
	L["Recipe Prefix Technique"] = "技巧：(%a+)"

	L["Tooltip Scan Skill Requirement"] = "需要(.+)（(%d+)）"
end