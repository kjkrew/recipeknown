--[[
	RecipeKnown
	v<%version%>
	
	Revision: $Id: Constants.lua 6 2014-12-16 13:30:27 PST Kjasi $
]]

RecipeKnown = {}
local rk = RecipeKnown

rk.Constants = {}
local c = rk.Constants

-- [Job skill req] = lockpicking skill
c.LockboxSkills_Blacksmith = {
	[100] = 25,			-- Silver Skeleton Key
	[150] = 125,		-- Golden Skeleton Key
	[200] = 200,		-- Truesilver Skeleton Key
	[275] = 300,		-- Arcanite Skeleton Key
	[350] = 375,		-- Cobalt Skeleton Key
	[430] = 400,		-- Titanium Skeleton Key
	[475] = 425,		-- Obsidium Skeleton Key
	[500] = 450,		-- Ghostly Skeleton Key
}
c.LockboxSkills_Engineer = {
	[100] = 150,		-- Small Seaforium Charge
	[200] = 250,		-- Large Seaforium Charge
	[275] = 300,		-- Powerful Seaforium Charge
	[350] = 350,		-- Elemental Seaforium Charge
	[425] = 400,		-- Volatile Seaforuium Blastpack
	[525] = 450,		-- Locksmith's Powderkeg
}

-- [Required Skill Level] = Spell ID
c.LockboxSkillRecipes_Blacksmith = {
	[100] = 19666,		-- Silver Skeleton Key
	[150] = 19667,		-- Golden Skeleton Key
	[200] = 19668,		-- Truesilver Skeleton Key
	[275] = 19669,		-- Arcanite Skeleton Key
	[350] = 59405,		-- Cobalt Skeleton Key
	[430] = 59406,		-- Titanium Skeleton Key
	[475] = 76438,		-- Obsidium Skeleton Key
	[500] = 122633,		-- Ghostly Skeleton Key
}

c.LockboxSkillRecipes_Engineer = {
	[100] = 3933,		-- Small Seaforium Charge
	[200] = 3972,		-- Large Seaforium Charge
	[275] = 23080,		-- Powerful Seaforium Charge
	[350] = 30547,		-- Elemental Seaforium Charge
	[425] = 84409,		-- Volatile Seaforuium Blastpack
	[525] = 127124,		-- Locksmith's Powderkeg
}

rk.Realm = GetRealmName()
rk.Faction = UnitFactionGroup("player")
rk.CharName = UnitName("player")
local _, cn = UnitClass("player")
rk.ClassName = cn

rk.Color1 = "|CFF5555FF"
rk.Color2 = "|CFF55FF55"

-- Item Type and Subtype indexes
-- These numbers match the order found in the auction house listings, as retrieved by GetAuctionItemClasses() and GetAuctionItemSubClasses(). They are used mainly for automatic localization.
rk.RecipeType = 7
rk.MiscType = 9
rk.MiscSubType = {}
rk.MiscSubType.Junk = 1		-- Lockboxes
rk.MiscSubType.Pets = 3		-- Companion Pets

rk.RecipeSubType = {}
-- Book = 1
rk.RecipeSubType.Leatherworking = 2
rk.RecipeSubType.Tailoring = 3
rk.RecipeSubType.Engineering = 4
rk.RecipeSubType.Blacksmithing = 5
rk.RecipeSubType.Cooking = 6
rk.RecipeSubType.Alchemy = 7
rk.RecipeSubType.FirstAid = 8
rk.RecipeSubType.Enchanting = 9
-- Fishing = 10
rk.RecipeSubType.Jewelcrafting = 11
rk.RecipeSubType.Inscription = 12

-- ProfIDs
rk.ProfID = {}
rk.ProfID.Alchemy			= 171
rk.ProfID.Blacksmithing		= 164
rk.ProfID.Cooking			= 185
rk.ProfID.Enchanting		= 333
rk.ProfID.Engineering		= 202
rk.ProfID.FirstAid			= 129
rk.ProfID.Inscription		= 773
rk.ProfID.Jewelcrafting		= 755
rk.ProfID.Leatherworking	= 165
rk.ProfID.Mining			= 186
rk.ProfID.Tailoring			= 197

rk.ProfSpellIDs = {}
rk.ProfSpellIDs.Alchemy			= 2259
rk.ProfSpellIDs.Blacksmithing	= 3100
rk.ProfSpellIDs.Cooking			= 2550
rk.ProfSpellIDs.Enchanting		= 7411
rk.ProfSpellIDs.Engineering		= 4036
rk.ProfSpellIDs.FirstAid		= 3273
rk.ProfSpellIDs.Inscription		= 45357
rk.ProfSpellIDs.Jewelcrafting	= 25229
rk.ProfSpellIDs.Leatherworking	= 2108
rk.ProfSpellIDs.Mining			= 2656
rk.ProfSpellIDs.Tailoring		= 3908

-- Cache
rk.Cache = {
	["items"] = {},	-- [itemid] = color array
	["jobs"] = {},		-- [profid] = array of chars with this job
	["lockpick"] = {},	-- [charname] = type & skill level of lockpicking ability
	--[""] = {},
}