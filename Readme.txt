RecipeKnown
Version <%version%>
<%date%>

Introduction:
RecipeKnown is a simple addon, designed to color the recipes you or your alts already know, allowing you to make faster and smarter decisions about what recipes to buy!


Acknowledgements:
 1) I'd like to thank Ayradyss for creating Grimoire Keeper. Without his work, I'd probably have never thought of making RecipeKnown.
 2) I gotta thank Bhim on ui.worldofwar.net for sending me the German Localization info!
 3) I want to thank ceric35 on wow.curse-gaming.com for helping me with the French localization!
 4) Thanks to Thargan on ui.worldofwar.net for pointing out that recipes can change their names when learned.
 5) Many thanks go to simshi on WoWInterface for the zhCN locale data!
 6) I HAVE to thank all the Beta Testers who've helped me improve RK beyond what I initially imagined! Thank you all!

-=To Do List=-
 - Rework the Scanning section a bit to use less memory and cpu cycles.
 - Add a "Gray" color to indicate recipes that none of your toons will ever have to learn.
 - Add a User Interface.
 - Add Command-line options.
 - You will be able to change the colors!
 - Modes will be officially dropped. Individual options will be in place for each of the colors.
 - Tooltip can be turned off, or not list a certain group. (Hiding the "Already Knows" group, for example.)
 - Add coloring support for the following windows: Mailbox, Bank, Guild Bank.


 -= History =-
v<%version%>
 - TOC Update for 6.1

v0.29
 - Update for 6.0.x
 - Removed Mounts and Pets from RK, since all characters now share pets. May re-implement in the future.

v0.28
 - 5.0.x TOC support.
 - Added 4.3-5.1 Pets.
 - Lockbox Tooltips will now state the profession/skill the person uses to open the box.
 - Lockbox Tooltips will now list people who can learn recipes that will open the box.
 - People with multiple skills that can open Lockboxes will now have all their skills shown. So Rogue Engineer Blacksmiths will have 3 listings on the Lockbox tooltips.
 - Fixed a lockbox bug that wouldn't list anyone unless at least 1 person could unlock the box.
 - Fixed a bug that would think the Rope Pet Leash was actually a pet.
 - Update and Scan functions will now properly update Cooking and First Aid.
 - Fixed a scanning bug.

v0.27
 - 4.2 Update
 - Tooltips now work with Pets! (No coloring yet.)
 - Updated Database.
 - RecipeKnown now works in the zhCN locale, thanks to simshi on WoWInterface!
 - Lockbox tooltips now list Blacksmiths and Engineers who can fashion keys or explosives that can open them.
 - Fixed some coloring issues. Recipes will now color the way they were meant to!
 - Fixed a bug where Recipes for pets would think they ARE pets.
 - Fixed a bug that would spam the unknown pet message. These messages will now display only once.
 - Removed Keys and their functions.

v0.26
 - Offical 4.0.x release.
 - Reenabled Rogue Lockpicking. (Still testing...)
 - No longer Beta release.

v0.25 Cata 01:
 - Initial Update for Cataclysm.
 - Tooltip now states how many toons out of total number know, can know or don't know a recipe.
 - Rogue Lockpicking tooltip disabled for now. (I hope to restore this soon!)
 - Class Skills disabled.

v0.25 Beta 5e:
 - Fixed a bug that wouldn't let recipes color.

v0.25 Beta 5d:
 - RecipeKnown's Tooltip now work with your rogue's lockpicking and lockboxes!
 - Added esES localization data. (Needs Testing)
 - Fixed a mispelling bug in the Database Update function.
 - Fixed a bug in the ItemID conversion function.

v0.25 Beta 5c:
 - Fixed the update function so it won't constantly remove the old options values, causing the coloring bug in Beta 5b.

v0.25 Beta 5b:
 - Fixed a coloring bug.
 - Fixed a bug that wouldn't let the database record a character's colors.

v0.25 Beta 5a:
 - Fixed a database bug that would occur if you didn't have an RK database already.
 - Changed the Options Database's format to reflect changes from Beta 6.

v0.25 Beta 5:
 - Updated ToC for Patch 3.3.
 - Fixed all the Reporting problems with Beta 4.
 - Made major changes to the database again. All previous data will be lost.
 - The Inscription tradeskill and associated techniques will now properly work.
 - Unlearning a tradeskill will now remove it from the character's RecipeKnown Database.
 - Changed the meaning of the "Blue" color. Now indicates recipes your alts will eventually be able to learn.
 - Changed the Detection method. This will help reduce low-frame rates.
 - Added a lot of new recipes to the Database. Database still needs to be sorted!
 - RK will now build an ItemId to Tradeskill ID database, based on your tradeskills, and their associated items. This will help speed up detection.
 - Reworked detection and coloring to be more universal and easier to manage.
 - Reworked how the tooltips operate to help reduce CPU usage.
 - Fixed a bug that occured when updating your Tradeskills, and no headers are present.


 --==Notes==--
 - There is a small "lag spike" when viewing recipes for the first time each session. This is because RK is building a color info database, which allows one of the new functionalities to work. The lag is worse if you have lots of toons that know the recipe skill being viewed. (IE: Larger lag spike when looking at Cooking recipes) I'm looking for a way to reduce this lag even more, hopefully removing it.
 - Merchants, Trading, and the Auction House are currently the only windows that color.
 - There is now a "Yellow" color to indicate recipes that your other toons can learn.
 - There is now a "Blue" color used for recipes that Alts will eventually be able to learn.
 - There is now a "Gray" color that tells you that the specified recipe will never be known, or have to be learned, by any of your toons.
 - Only Recipes and BC Keys color at the moment. Class skills/books are pending.
 - English is fully supported.
 - French & German functionality has been installed. Cosmetic translations (found in localization.en.lua) requested.
 - Functionality & Cosmetic translations for other locales requested.