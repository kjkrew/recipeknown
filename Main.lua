--[[
	RecipeKnown Main LUA File
	v<%version%>

	Revision: $Id: Main.lua 19 2014-12-16 15:00:03 PST Kjasi $
]]

local rk = _G.RecipeKnown
if (not rk) then
	print(RED_FONT_COLOR_CODE.."Main is unable to find RecipeKnown's Global."..FONT_COLOR_CODE_CLOSE)
	return
end
local L = rk.Localization
if (not L) then
	print(RED_FONT_COLOR_CODE.."Main is unable to find RecipeKnown's Localization data."..FONT_COLOR_CODE_CLOSE)
	return
end


rk.Version = "<%version%>"

-- Libraries
local LibKJ = LibStub("LibKjasi-1.0")
local LCI = LibStub("LibCraftInfo-1.0")

-- Setup our addon with KjasiLib
LibKJ:setupAddon({
	["Name"] = L["RK Title"],
	["Version"] = rk.Version,
	["MsgStrings"] = {
		["Default"] = L["Message"],
		["error"] = L["Message Error"],
	},
	["ChannelLevel"] = 0,
})

rk.ErrorList = {}

local db_cutoff = {
	["CharData"] = "0.25 Cata 01",
	["Options"] = "0.25 Cata 01",
}

rk.Defaults = {
	["Options"] = {
		["Version"] = rk.Version,
		["Toggles"] = {},
		["Colors"] = {},
	},
	["Colors"] = {
		["Known"] = {0.1,1,0.1},
		["KnownAlt"] = {0.5,0.75,0.5},
		["Learn"] = {1,1,1},
		["LearnAlt"] = {1,1,0},
		["LearnAltLater"] = {0.1,0.5,0.75},
		["NeverLearn"] = {0.35,0.35,0.35},
	},
	["Toggles"] = {
		["Known"] = 1,
		["KnownAlt"] = 1,
		["Learn"] = 1,
		["LearnAlt"] = 1,
		["LearnAltLater"] = 0,
		["NeverLearn"] = 0,
	},
}

-- Events List
local EventList = {
	-- 1-5 Detection Events
	"TRADE_SKILL_SHOW",
	"TRADE_SKILL_UPDATE",
	"PLAYER_LEVEL_UP",
	"CHAT_MSG_SYSTEM",
	"CHAT_MSG_SKILL",

	-- 6-7 Merchant Events
	"MERCHANT_SHOW",
	"MERCHANT_UPDATE",

	-- 8-11 Auction House Events
	"AUCTION_HOUSE_SHOW",
	"AUCTION_ITEM_LIST_UPDATE",
	"AUCTION_BIDDER_LIST_UPDATE",
	"AUCTION_OWNED_LIST_UPDATE",

	-- 12-14 Trading Events
	"TRADE_PLAYER_ITEM_CHANGED",
	"TRADE_TARGET_ITEM_CHANGED",
	"TRADE_CLOSED",

	-- 15 Companions
	"COMPANION_LEARNED",
	"PET_JOURNAL_LIST_UPDATE",
}

--== Common Functions ==--
function rk:Msg(msg,channel)
	if (msg == nil) then return end
	LibKJ:Msg(L["RK Title"], msg, channel)
end

function rk:OnLoad(self)
	-- Event Registration
	self:RegisterEvent("PLAYER_LOGIN")
	foreach(EventList,function(k,v) self:RegisterEvent(v) end)

	-- Slash Commands
	SLASH_RecipeKnown1 = "/recipeknown"
	SLASH_RecipeKnown2 = "/rk"
	SlashCmdList["RecipeKnown"] = RecipeKnown_commandline
end

function rk:OnEvent(self,event,...)
	local arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17, arg18 = ...

	--rk:Msg("Event: "..tostring(event).."\r  arg1:"..tostring(arg1)..", arg2:"..tostring(arg2)..", arg3:"..tostring(arg3).."\r  arg4:"..tostring(arg4)..", arg5:"..tostring(arg5)..", arg6:"..tostring(arg6).."\r  arg7:"..tostring(arg7)..", arg8:"..tostring(arg8)..", arg9:"..tostring(arg9).."\r  arg10:"..tostring(arg10)..", arg11:"..tostring(arg11)..", arg12:"..tostring(arg12).."\r  arg13:"..tostring(arg13)..", arg14:"..tostring(arg14)..", arg15:"..tostring(arg15).."\r  arg16:"..tostring(arg16)..", arg17:"..tostring(arg17)..", arg18:"..tostring(arg18))

	if (event == "PLAYER_LOGIN") then
		RecipeKnown_Loaded()
	elseif (event == "CHAT_MSG_SYSTEM") then
		--rk:Msg("string:"..tostring(arg1)..", Match:"..tostring(strmatch(arg1,gsub(ERR_SPELL_UNLEARNED_S, "%%s", "%%a+"))))
		if (strmatch(arg1,gsub(ERR_LEARN_RECIPE_S, "%%s", "%%a+"))) then
			--rk:Msg("You have Learned something!")
			if (TradeSkillFrame)and(TradeSkillFrame:IsVisible()) then
				rk:Scanner("Recipes")
			end
		elseif (strmatch(arg1,gsub(ERR_SPELL_UNLEARNED_S, "%%s", "%%a+"))) then
			--rk:Msg("You have Unlearned something!")
			for k,skill in pairs(rk.Skills) do
				if (strfind(arg1,skill)) then
					RecipeKnown_UnlearnSkill(skill)
					break
				end
			end
		end
	elseif (event == "CHAT_MSG_SKILL") then
		rk:ClearCache("items")
		rk:Scanner("Spells")
	elseif (event == "TRADE_SKILL_SHOW") then
		RecipeKnown_ScanData_Build()
		rk:Scanner("Recipes")
	elseif  (event == "TRADE_SKILL_UPDATE") then
		rk:Scanner("Recipes")
	elseif (event == "AUCTION_HOUSE_SHOW") then
		RecipeKnown_ScanData_Build()
	elseif (event == "MERCHANT_SHOW") then
		self:UnregisterEvent("MERCHANT_UPDATE")
		RecipeKnown_ScanData_Build()
		RecipeKnown_ScanItems(RecipeKnown_ScanData("Merchant"))
		self:RegisterEvent("MERCHANT_UPDATE")
	elseif (event == "MERCHANT_UPDATE") then
		self:UnregisterEvent("MERCHANT_SHOW")
		RecipeKnown_ScanItems(RecipeKnown_ScanData("Merchant"))
		self:RegisterEvent("MERCHANT_SHOW")
	elseif (event == "AUCTION_ITEM_LIST_UPDATE") or (event == "AUCTION_BIDDER_LIST_UPDATE") or (event == "AUCTION_OWNED_LIST_UPDATE") then
		self:UnregisterEvent("AUCTION_ITEM_LIST_UPDATE")
		self:UnregisterEvent("AUCTION_BIDDER_LIST_UPDATE")
		self:UnregisterEvent("AUCTION_OWNED_LIST_UPDATE")
		RecipeKnown_ScanItems(RecipeKnown_ScanData("Auction"))
		self:RegisterEvent("AUCTION_ITEM_LIST_UPDATE")
		self:RegisterEvent("AUCTION_BIDDER_LIST_UPDATE")
		self:RegisterEvent("AUCTION_OWNED_LIST_UPDATE")
	elseif (event == "PLAYER_LEVEL_UP") then
		RecipeKnown_CharData[rk.Realm][rk.Faction]["data"][rk.CharName]["Level"] = tostring(arg1)
		rk:Scanner("Spells")
	elseif (event == "TRADE_PLAYER_ITEM_CHANGED") then
		self:UnregisterEvent("TRADE_PLAYER_ITEM_CHANGED")
		RecipeKnown_ColorClean("TradePlayer", arg1)
		RecipeKnown_ScanItems(RecipeKnown_ScanData("TradePlayer"))
		self:RegisterEvent("TRADE_PLAYER_ITEM_CHANGED")
	elseif (event == "TRADE_TARGET_ITEM_CHANGED") then
		self:UnregisterEvent("TRADE_TARGET_ITEM_CHANGED")
		RecipeKnown_ColorClean("TradeTarget", arg1)
		RecipeKnown_ScanItems(RecipeKnown_ScanData("TradeTarget"))
		self:RegisterEvent("TRADE_TARGET_ITEM_CHANGED")
	elseif (event == "TRADE_CLOSED") then
		for x=1,MAX_TRADE_ITEMS do
			RecipeKnown_ColorClean("TradePlayer", x)
			RecipeKnown_ColorClean("TradeTarget", x)
		end
	end
end

function rk:ClearCache(clearing)
	if (clearing == "items") then
		rk.Cache["items"] = {}
	elseif (clearing == "jobs") then
		rk.Cache["jobs"] = {}
	elseif (clearing == "lockpick") then
		rk.Cache["lockpick"] = {}
	else
		rk.Cache = {
			["items"] = {},
			["jobs"] = {},
			["lockpick"] = {},
		}
	end
end

function RecipeKnown_Loaded()
	RecipeKnown_GenerateDatabase()
--	RecipeKnown_Database_Update()
	
	--== Function Hooks ==--
	-- Load AuctionUI
	local LoadAddon = UIParentLoadAddOn("Blizzard_AuctionUI")

	-- Merchant Window
	RecipeKnown_Orig_MerchantFrame_UpdateMerchantInfo = MerchantFrame_UpdateMerchantInfo
	MerchantFrame_UpdateMerchantInfo = RecipeKnown_MerchantFrame_Update

	-- Auction Browse Window
	RecipeKnown_Orig_AuctionFrameBrowse_Update = AuctionFrameBrowse_Update
	AuctionFrameBrowse_Update = RecipeKnown_AuctionFrameBrowse_Update

	-- Auction Bid Window
	RecipeKnown_Orig_AuctionFrameBid_Update = AuctionFrameBid_Update
	AuctionFrameBid_Update = RecipeKnown_AuctionFrameBid_Update

	-- Auction Owner Window
	RecipeKnown_Orig_AuctionFrameAuctions_Update = AuctionFrameAuctions_Update
	AuctionFrameAuctions_Update = RecipeKnown_AuctionFrameAuctions_Update

	-- Auction Tab Button
	RecipeKnown_Orig_AuctionFrameTab_OnClick = AuctionFrameTab_OnClick
	AuctionFrameTab_OnClick = RecipeKnown_AuctionFrameTab_OnClick

	-- Tooltips
	GameTooltip:HookScript("OnTooltipSetItem", RecipeKnown_Tooltip_Load)
	GameTooltip:HookScript("OnTooltipCleared", RecipeKnown_Tooltip_Hide)
	ItemRefTooltip:HookScript("OnTooltipSetItem", RecipeKnown_Tooltip_Load)
	ItemRefTooltip:HookScript("OnTooltipCleared", RecipeKnown_Tooltip_Hide)
	

	--== Addon Hooks ==--
	-- Atlasloot
	if (AtlasLoot) then
		if AtlasLootTooltipTEMP then
			AtlasLootTooltipTEMP:HookScript("OnTooltipSetItem", RecipeKnown_Tooltip_Load)
			AtlasLootTooltipTEMP:HookScript("OnTooltipCleared", RecipeKnown_Tooltip_Hide)
		end
	end


--[[

	hooksecurefunc(ItemRefTooltip, "Show", RecipeKnown_Tooltip_Load)
	hooksecurefunc(GameTooltip, "Show", RecipeKnown_Tooltip_Load)
	hooksecurefunc(ItemRefTooltip, "Hide", RecipeKnown_Tooltip_Hide)
	hooksecurefunc(GameTooltip, "Hide", RecipeKnown_Tooltip_Hide)

	hooksecurefunc(ItemRefTooltip, "SetHyperlink", RecipeKnown_Tooltip_SetHyperlink)
	hooksecurefunc(GameTooltip, "SetHyperlink", RecipeKnown_Tooltip_SetHyperlink)

	hooksecurefunc(GameTooltip, "SetAuctionItem",		RecipeKnown_Tooltip_Load)
	hooksecurefunc(GameTooltip, "SetBagItem",			RecipeKnown_Tooltip_Load)
	hooksecurefunc(GameTooltip, "SetBuybackItem",		RecipeKnown_Tooltip_Load)
	hooksecurefunc(GameTooltip, "SetGuildBankItem",		RecipeKnown_Tooltip_Load)
	hooksecurefunc(GameTooltip, "SetInboxItem",			RecipeKnown_Tooltip_Load)
	hooksecurefunc(GameTooltip, "SetLootItem",			RecipeKnown_Tooltip_Load)
	hooksecurefunc(GameTooltip, "SetLootRollItem",		RecipeKnown_Tooltip_Load)
	hooksecurefunc(GameTooltip, "SetMerchantItem",		RecipeKnown_Tooltip_Load)
	hooksecurefunc(GameTooltip, "SetQuestItem",			RecipeKnown_Tooltip_Load)
	hooksecurefunc(GameTooltip, "SetQuestLogItem",		RecipeKnown_Tooltip_Load)
	hooksecurefunc(GameTooltip, "SetSendMailItem",		RecipeKnown_Tooltip_Load)
	hooksecurefunc(GameTooltip, "SetTradePlayerItem",	RecipeKnown_Tooltip_Load)
	hooksecurefunc(GameTooltip, "SetTradeTargetItem",	RecipeKnown_Tooltip_Load)

]]

	rk:Scanner("Spells")
	rk:Msg(format(L["Is Now Loaded"],rk.Version))
end

-- ItemID Function
function rk:GetItemID(itemlink)
	if (not itemlink) or (itemlink == nil) then
		return
	end
	
	--rk:Msg("Processing "..itemlink)
	--print("Returning: "..LibKJ:getIDNumber(itemlink))
	
	return LibKJ:getIDNumber(itemlink)
end

-- Update Windows
function RecipeKnown_UpdateWindows()
	RecipeKnown_ScanData_Build()
	if (MerchantFrame) and (MerchantFrame:IsVisible()) then
		RecipeKnown_ScanItems(RecipeKnown_ScanData("Merchant"))
	end
	if ((AuctionFrameBrowse) and (AuctionFrameBrowse:IsVisible())) or ((AuctionFrameBid) and (AuctionFrameBid:IsVisible())) or ((AuctionFrameAuctions) and (AuctionFrameAuctions:IsVisible())) then
		RecipeKnown_ScanItems(RecipeKnown_ScanData("Auction"))
	end
end

--== Database Updating Functions ==--
function RecipeKnown_Database_Update(isdbupdate)
	if (not isdbupdate) then
		if (not RecipeKnown_Options) or (not RecipeKnown_CharData) then
			RecipeKnown_GenerateDatabase()
			return
		end
		if (not RecipeKnown_Options["Version"]) or (RecipeKnown_Options["Version"] <= db_cutoff["Options"]) or (RecipeKnown_Options["Version"] <= db_cutoff["CharData"]) or (RecipeKnown_Options["Colors"] == {})  then
			RecipeKnown_GenerateDatabase()
			return
		end
	end

	local temp = rk.Defaults["Options"]
	local db = RecipeKnown_Options
	
	RecipeKnown_Options = LibKJ:UpdateDatabase(db,temp)
	RecipeKnown_Options["Version"] = rk.Version
end

function RecipeKnown_GenerateDatabase()
	local cutoff = false
	if (RecipeKnown_Options) and (RecipeKnown_Options["Version"]) then
		if (RecipeKnown_Options["Version"] <= db_cutoff["CharData"]) then
			rk:Msg(L["Error Database Character Data Deleted"],"error")
			RecipeKnown_CharData = nil
			cutoff = true
		end
		if (RecipeKnown_Options["Version"] <= db_cutoff["Options"]) then
			rk:Msg(L["Error Database Options Deleted"],"error")
			RecipeKnown_Options = nil
			cutoff = true
		end
	end

	if (not RecipeKnown_ItemID2TsID) then
		RecipeKnown_ItemID2TsID = {}
	end

	if (not RecipeKnown_CharData) then
		RecipeKnown_CharData = {
			["pets"] = {}
		}
	end
	if (not RecipeKnown_CharData[rk.Realm]) then
		RecipeKnown_CharData[rk.Realm] = {}
	end
	if (not RecipeKnown_CharData[rk.Realm][rk.Faction]) then
		RecipeKnown_CharData[rk.Realm][rk.Faction] = {
			["data"] = {},
			["recipes"] = {},
			["spells"] = {},
		}
	end
	-- Items that were added after the first version
	if (not RecipeKnown_CharData[rk.Realm][rk.Faction]["spells"]) then
		RecipeKnown_CharData[rk.Realm][rk.Faction]["spells"] = {}
	end
	
	-- Add initial data
	if (not RecipeKnown_CharData["pets"]) then
		RecipeKnown_CharData["pets"] = {}
	end
	if (not RecipeKnown_CharData[rk.Realm][rk.Faction]["data"][rk.CharName]) then
		RecipeKnown_CharData[rk.Realm][rk.Faction]["data"][rk.CharName] = {
			["Level"] = UnitLevel("player"),
			["Race"] = UnitRace("player"),
			["Class"] = rk.ClassName,
			["Professions"] = {},
		}
	end

	-- Remove old data
	if (RecipeKnown_CharData[rk.Realm][rk.Faction]["pets"]) then
		RecipeKnown_CharData[rk.Realm][rk.Faction]["pets"] = {}
	end

	if (not RecipeKnown_Options) or (not RecipeKnown_Options["Version"]) then
		--rk:Msg("Setting Options Defaults")
		RecipeKnown_Options = rk.Defaults["Options"]
	end

	if (not RecipeKnown_Options["Toggles"]) then
		--rk:Msg("Creating Toggles Group")
		RecipeKnown_Options["Toggles"] = {}
	end
	if (not RecipeKnown_Options["Toggles"][rk.Realm]) then
		--rk:Msg("Setting Toggles Realm for Realm:"..tostring(rk.Realm))
		RecipeKnown_Options["Toggles"][rk.Realm] = {}
	end
	if (not RecipeKnown_Options["Toggles"][rk.Realm][rk.CharName]) then
		--rk:Msg("Setting Toggles Default for Char:"..tostring(rk.CharName))
		RecipeKnown_Options["Toggles"][rk.Realm][rk.CharName] = rk.Defaults["Toggles"]
	end

	if (LibKJ:tcount(RecipeKnown_Options["Colors"]) ~= 6) then
		--rk:Msg("Creating Colors Group")
		RecipeKnown_Options["Colors"] = rk.Defaults["Colors"]
	end

	-- Removes the previous format into the new one.
	if (RecipeKnown_Options["Colors"][rk.Realm]) and (RecipeKnown_Options["Colors"][rk.Realm][rk.CharName]) then
		RecipeKnown_Options["Colors"] = RecipeKnown_Options["Colors"][rk.Realm][rk.CharName]
	end
	if (RecipeKnown_Options["Colors"][rk.Realm]) then
		RecipeKnown_Options["Colors"] = rk.Defaults["Colors"]
	end

	if (cutoff == false) and (RecipeKnown_Options["Version"] <= rk.Version) then
		RecipeKnown_Database_Update(1)
	end
end

function RecipeKnown_commandline(cmd)
	cmd = strlower(cmd)

--	rk:Msg("Currently no commandline options available.")

	if (cmd == "a") then
		RecipeKnown_Timer("Test1", 0.1, function() rk:Msg("Timer 0.1") end)
		RecipeKnown_Timer("Test2", 0.5, function() rk:Msg("Timer 0.5") end)
		RecipeKnown_Timer("Test3", 1, function() rk:Msg("Timer 1") end)
		RecipeKnown_Timer("Test4", 2, function() rk:Msg("Timer 2") end)
	elseif (cmd == "b") then
		local profarray = { GetProfessions() }
		for k, profID in pairs(profarray) do
			local name, icon, skillLevel, maxSkillLevel, return5, return6, return7, r8, r9 = GetProfessionInfo(profID)
			rk:Msg(format("ProfInfo: ID: %s, name: %s, R5: %s, R6: %s, R7: %s, R8: %s, R9: %s", tostring(profID), tostring(name), tostring(return5), tostring(return6), tostring(return7), tostring(r8), tostring(r9)))
		end
	elseif (cmd == "c") then
		RecipeKnown_ScanPets()
	else
		rk:Msg("Command Not Found")
	end

end

function RecipeKnown_UnlearnSkill(skill)
	-- rk:Msg("Killing Skill:"..skill)
	RecipeKnown_GenerateDatabase()

	local db = RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"]
	local temp, jobtemp = {}, {}
	local jobcount, tooncount = 0,0

	if (not db[skill]) then
		--rk:Msg("Skill not found. Killing...")
		return
	end 

	for job,v in pairs(db) do
		if (skill == job) then
			for toon, v2 in pairs(db[job]) do
				if (toon ~= rk.CharName) then
					tooncount = tooncount + 1
					temp[toon] = v2
				end
			end
		else
			jobtemp[job] = v
			jobcount = jobcount + 1
		end
	end

	if (jobcount == 0) then
		RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"] = {}
	elseif (tooncount == 0) then
		RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"] = jobtemp
	else
		RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][skill] = temp
	end
	RecipeKnown_CharData[rk.Realm][rk.Faction]["data"][rk.CharName]["Professions"] = {}

	-- rk:Msg("Done! ["..skill.."]="..tostring(RecipeKnown_CharData[rk.Realm][rk.Faction][skill][rk.CharName]))
end

-- Timer function
function RecipeKnown_Timer(name,targettime,functiontodo,...)
	LibKJ:Timer(L["RK Title"],name,targettime,functiontodo,...)
end

--== Hook Functions ==--
-- Merchant Updater
function RecipeKnown_MerchantFrame_Update()
	RecipeKnown_Orig_MerchantFrame_UpdateMerchantInfo()
	RecipeKnown_ScanItems(RecipeKnown_ScanData("Merchant"))
end

-- Auction Browse Updated
function RecipeKnown_AuctionFrameBrowse_Update()
	--rk:Msg("Running Auction Update Function")
	RecipeKnown_Orig_AuctionFrameBrowse_Update()
	RecipeKnown_ScanItems(RecipeKnown_ScanData("Auction"))
end

-- Auction Bid Updated
function RecipeKnown_AuctionFrameBid_Update()
	--rk:Msg("Running Auction Update Function")
	RecipeKnown_Orig_AuctionFrameBid_Update()
	RecipeKnown_ScanItems(RecipeKnown_ScanData("Auction"))
end

-- Auction Owner Updated
function RecipeKnown_AuctionFrameAuctions_Update()
	--rk:Msg("Running Auction Update Function")
	RecipeKnown_Orig_AuctionFrameAuctions_Update()
	RecipeKnown_ScanItems(RecipeKnown_ScanData("Auction"))
end

-- Auction Tab Change
function RecipeKnown_AuctionFrameTab_OnClick(self, index)
	RecipeKnown_Orig_AuctionFrameTab_OnClick(self, index)

	RecipeKnown_ScanItems(RecipeKnown_ScanData("Auction"))
end