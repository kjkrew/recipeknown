--[[
	RecipeKnown Color Gnomes functions
	v<%version%>
	
	Revision: $Id: colorgnomes.lua 16 2014-12-16 14:37:26 PST Kjasi $
]]

local rk = _G.RecipeKnown
if (not rk) then
	print(RED_FONT_COLOR_CODE.."Color Gnomes are unable to find RecipeKnown's Global."..FONT_COLOR_CODE_CLOSE)
	return
end
local L = rk.Localization
if (not L) then
	print(RED_FONT_COLOR_CODE.."Color Gnomes are unable to find RecipeKnown's Localization data."..FONT_COLOR_CODE_CLOSE)
	return
end

local error_color = {0.9999,0.9999,0.9999}	-- Virtually {1,1,1}, but allows us to detect errors.

local backgrounds = {
	["NameFrame"] = {0.5,0.5,0.5},
}
local nodata = false
local ItemColorData = {}

-- Returns color array
function rk:GetItemColor(itemid)
	if (rk.Cache["items"][itemid]) then
		return rk.Cache["items"][itemid]
	end
	
	-- Item is not in the cache, so we must figure out the color
	RecipeKnown_GenerateDatabase() -- Just in case...
	local color = error_color
	local _, link = GetItemInfo(itemid)
	local isknown, canlearn = false, false
	local kcount, ckcount, dkcount, tooncount = 0,0,0,0
	local type = rk:GetItemType(link)
	
	for _,_ in pairs(RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"]) do
		tooncount = tooncount + 1
	end
	
	if (type == "recipe") then
		local job, reqskill = RecipeKnown_Tooltip_GetJob(link)
		local db = RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"]
		local tsid = rk:Link2TsID(itemid)
		
		if (not db) then
			return color
		end
		if (not db[job]) then
			color = RecipeKnown_Options["Colors"]["NeverLearn"]
			rk.Cache["items"][itemid] = color
			return color
		end
		
		for toon,v in pairs(db[job]) do
			local tmin = v["data"]["current"]
			
			if (v["recipes"][tsid]) then
				kcount = kcount + 1
			elseif (tmin >= tonumber(reqskill)) then
				ckcount = ckcount + 1
			elseif (tmin < tonumber(reqskill)) then
				dkcount = dkcount + 1
			end
		end
		
		if (tsid ~= nil) then
			if (db1[job]) and (db1[job][rk.CharName]) and (db1[job][rk.CharName]["recipes"][tsid]) then
				isknown = true
			elseif (db1[job]) and (db1[job][rk.CharName]) then
				canlearn = true
			end
		end
	end
	
	if (isknown) then
		color = RecipeKnown_Options["Colors"]["Known"]
		if (dkcount > 0) then
			color = RecipeKnown_Options["Colors"]["LearnAltLater"]
		end
		if (ckcount > 0) then
			color = RecipeKnown_Options["Colors"]["LearnAlt"]
		end
	else
		if (kcount > 0) then
			color = RecipeKnown_Options["Colors"]["KnownAlt"]
		end
		if (dkcount > 0) then
			color = RecipeKnown_Options["Colors"]["LearnAltLater"]
		end
		if (ckcount > 0) then
			color = RecipeKnown_Options["Colors"]["LearnAlt"]
		end
		if (canlearn == true) then
			color = RecipeKnown_Options["Colors"]["Learn"]
		end
	end

	if (color ~= colorerror) then
		rk.Cache["items"][itemid] = color
	end
	return color
end

function rk:GetItemType(link)
	local t = "none"
	local _,_,_,_,_,itemtype,itemsubtype = GetItemInfo(link)
	
	if (itemtype == L["Itemtype Recipe"]) then
		t = "recipe"
	end
	
	return t
end

function rk:ColorGnomes(itemid,icon,background)
	local c = rk:GetItemColor(itemid)
	
	if (not c) or (c == error_color) then
		return
	end
	
	icon:SetVertexColor(c)
	background:SetVertexColor(c)
end

function rk:ColorFrame(self)
	local fname = self:GetName()
	local numslots, icon, background, getlink
	
	if (fname == "MerchantFrame") then
		numslots = MERCHANT_ITEMS_PER_PAGE
		icon = "MerchantItem%iItemButton"
		background = "MerchantItem%iSlotTexture"
		getlink = "GetMerchantItemLink(%i)"
	end
	for i=1,numslots do
		local slotn = format(slotname,i)
		local item = _G[format(getlink,i)]
		
	end
end

function RecipeKnown_ColorGnomes(info, colorlist, colordata, type)
	rk:Msg("Running Color Gnomes!")

	for slot,itemid in pairs(colorlist) do
		local trueslot = slot
		while (trueslot > info["perpage"]) do
			trueslot = trueslot - info["perpage"]
		end

		for k,layer in pairs(info["colorlist"]) do
			local bg = nil
			rk:Msg("Trying to find:"..info["itemname"]..trueslot..layer.." for item slot:"..slot)
			local thisitem = getglobal(info["itemname"]..trueslot..layer)
			if (thisitem ~= nil) then
				rk:Msg("Found! Coloring...")
				local color = rk.GetItemColor(itemid)
				if backgrounds[layer] then
					thisitem:SetVertexColor(backgrounds[layer])
				else
					thisitem:SetVertexColor(color)
				end
				if (nodata == true) then
					SetDesaturation(thisitem, 1)
				end
			end
		end
	end
end

function RecipeKnown_ColorClean(ScanType, slot)
	local info = RecipeKnown_ScanData(ScanType)

	local trueslot = slot
	while (trueslot > info["perpage"]) do
		trueslot = trueslot - info["perpage"]
	end
	
	for k2,layer in pairs(info["colorlist"]) do
		local thisitem = getglobal(info["itemname"]..trueslot..layer)

		thisitem:SetVertexColor(1,1,1)
	end
end

function RecipeKnown_BuildItemColorData(link)
	if (link == nil) then return end
	local _,link,_,_,_,itype = GetItemInfo(link)
	local id = rk:GetItemID(link)

	-- rk:Msg("Build ID:"..tostring(id))

	if (not id) or (itype ~= L["Itemtype Recipe"]) then return end

	if (not ItemColorData[id]) then
		-- rk:Msg("Building item data for:"..link)
		local _, _, _, k, ck, dk = RecipeKnown_Tooltip_GetInfo(link)

		ItemColorData[id] = {
			["Known"] = tonumber(k) or 0,
			["CanKnow"] = tonumber(ck) or 0,
			["DontKnow"] = tonumber(dk) or 0,
		}
	end

	-- rk:Msg("Build Result:"..tostring(ItemColorData[id]["CanKnow"]))
end