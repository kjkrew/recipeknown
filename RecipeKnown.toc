## Interface: 60100
## Title: RecipeKnown
## Author: Kjasi
## Version: <%version%>
## Notes: Colors recipes that you and your alts already know.
## DefaultState: Enabled
## LoadOnDemand: 0
## URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Website: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Feedback: https://bitbucket.org/Kjasi/kjasis-wow-addons
## SavedVariables: RecipeKnown_Options, RecipeKnown_CharData, RecipeKnown_ItemID2TsID
## OptionalDeps: Auctioneer

Load.xml