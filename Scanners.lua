--[[
	RecipeKnown Scanners & Scan Data
	v<%version%>
	
	Revision: $Id: Scanners.lua 17 2014-12-16 14:35:00 PST Kjasi $
]]

local rk = _G.RecipeKnown
if (not rk) then
	print(RED_FONT_COLOR_CODE.."Scanners are unable to find RecipeKnown's Global."..FONT_COLOR_CODE_CLOSE)
	return
end
local L = rk.Localization
if (not L) then
	print(RED_FONT_COLOR_CODE.."Scanners are unable to find RecipeKnown's Localization data."..FONT_COLOR_CODE_CLOSE)
	return
end

local db = rk.Database

local idarray, namearray = {}, {}
local arraycount = 0

--== Scan Data functions ==--
-- Clear Search Arrays
function RecipeKnown_ScanData_Clear()
	idarray = {}
	namearray = {}
	arraycount = 0
end

-- Build searchable arrays
function RecipeKnown_ScanData_Build()
	RecipeKnown_ScanData_Clear()
	if (not RecipeKnown_CharData[rk.Realm][rk.Faction]) then return end
	for job,v in pairs(RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"]) do
		for toon,v2 in pairs(v) do
			for tsid, name in pairs(v2["recipes"]) do
				if (not idarray[tsid]) then
					idarray[tsid] = {
						["name"] = name,
						["skill"] = job,
						["toons"] = {},
					}
					arraycount = arraycount + 1
				end

				idarray[tsid]["toons"][toon] = v[toon]["data"]["current"]
				if (not namearray[name]) then
					namearray[name] = tsid
				end
			end
		end
	end
end

--== The Data Scanner Functions ==--

-- Update the Profession Numbers
function RecipeKnown_UpdateNumbers()
	local prof1, prof2, archeology, fishing, cooking, firstaid = GetProfessions()
	RecipeKnown_GenerateDatabase()

	if (prof1) then
		local name, texture, rank, maxRank, numSpells, spelloffset, skillLine, rankModifier, specializationIndex, specializationOffset = GetProfessionInfo(prof1)
		RecipeKnown_BuildNumbersDB(name,rank,maxRank)
	end
	if (prof2) then
		local name, texture, rank, maxRank, numSpells, spelloffset, skillLine, rankModifier, specializationIndex, specializationOffset = GetProfessionInfo(prof2)
		RecipeKnown_BuildNumbersDB(name,rank,maxRank)
	end
	if (cooking) then
		local name, texture, rank, maxRank, numSpells, spelloffset, skillLine, rankModifier, specializationIndex, specializationOffset = GetProfessionInfo(cooking)
		RecipeKnown_BuildNumbersDB(name,rank,maxRank)
	end
	if (firstaid) then
		local name, texture, rank, maxRank, numSpells, spelloffset, skillLine, rankModifier, specializationIndex, specializationOffset = GetProfessionInfo(firstaid)
		RecipeKnown_BuildNumbersDB(name,rank,maxRank)
	end
end

-- Gathers recipe info for all professions.
function RecipeKnown_Scan_AllRecipes()
	local prof1, prof2, archeology, fishing, cooking, firstaid = GetProfessions()
	RecipeKnown_GenerateDatabase()

	if (prof1) then
		local name, texture, rank, maxRank, numSpells, spelloffset, skillLine, rankModifier, specializationIndex, specializationOffset = GetProfessionInfo(prof1)
		RecipeKnown_BuildNumbersDB(name,rank,maxRank)
	end
	if (prof2) then
		local name, texture, rank, maxRank, numSpells, spelloffset, skillLine, rankModifier, specializationIndex, specializationOffset = GetProfessionInfo(prof2)
		RecipeKnown_BuildNumbersDB(name,rank,maxRank)
	end
	if (cooking) then
		local name, texture, rank, maxRank, numSpells, spelloffset, skillLine, rankModifier, specializationIndex, specializationOffset = GetProfessionInfo(cooking)
		RecipeKnown_BuildNumbersDB(name,rank,maxRank)
	end
	if (firstaid) then
		local name, texture, rank, maxRank, numSpells, spelloffset, skillLine, rankModifier, specializationIndex, specializationOffset = GetProfessionInfo(firstaid)
		RecipeKnown_BuildNumbersDB(name,rank,maxRank)
	end
end

--Recipe Gatherer
function RecipeKnown_Scan_Recipes(ProfID)
	local name = GetProfessionInfo(ProfID)

end

-- Update Professions Database
function RecipeKnown_BuildNumbersDB(name,skillLevel,maxSkillLevel)
	-- Build DB if it doesn't exist.
	if (not RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][name]) then
		RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][name] = {}
	end
	if (not RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][name][rk.CharName]) then
		RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][name][rk.CharName] = {}
	end
	if (not RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][name][rk.CharName]["data"]) then
		RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][name][rk.CharName]["data"] = {
			["current"] = skillLevel,
			["max"] = maxSkillLevel,
		}
	end
	if (not RecipeKnown_CharData[rk.Realm][rk.Faction]["data"][rk.CharName]["Professions"][name]) then
		RecipeKnown_CharData[rk.Realm][rk.Faction]["data"][rk.CharName]["Professions"][name] = {
			["skilllvl"] = skillLevel,
			["spec"] = "none",
		}
	end

	-- Update existing DBs
	RecipeKnown_CharData[rk.Realm][rk.Faction]["data"][rk.CharName]["Professions"][name]["skilllvl"] = skillLevel
	RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][name][rk.CharName]["data"]["current"] = skillLevel
	RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][name][rk.CharName]["data"]["max"] = maxSkillLevel
	
	-- Update Specialties
	if (IsSpellKnown(20219)) then
		RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][name][rk.CharName]["data"]["spec"] = GetSpellInfo(20219)
	elseif (IsSpellKnown(20222)) then
		RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][name][rk.CharName]["data"]["spec"] = GetSpellInfo(20222)
	elseif (RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][name][rk.CharName]["data"]["spec"]) then
		local temp = {}
		for k,v in pairs(RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][name][rk.CharName]["data"]) do
			if (k ~= "spec") then
				temp[k] = v
			end
		end
		RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][name][rk.CharName]["data"] = temp
	end
end

-- Old Method
function rk:Scanner(ScanType)
	--rk:Msg("Scanner Running...")
	if (ScanType == "Spells") then
		if (rk.ClassName == "DRUID") then
		elseif (rk.ClassName == "MAGE") then
		elseif (rk.ClassName == "PREIST") then
		elseif (rk.ClassName == "ROGUE") then
		elseif (rk.ClassName == "SHAMAN") then
		elseif (rk.ClassName == "WARLOCK") then
		end
	elseif (ScanType == "Recipes") then
		-- if Tradeskill frame is hidden, if it's a linked Tradeskill or the "Have Materials" button is checked, then fail.
		if (not TradeSkillFrame) or (not TradeSkillFrame:IsVisible()) or (IsTradeSkillLinked()) or (IsTradeSkillGuild()) then
			--rk:Msg("Initial Failure: Tradeskill window not found, not visible, is linked.")
			return
		end

		--rk:Msg("Scanning Recipes...")

		local tradename, tradecurrentskill, trademaxskill = GetTradeSkillLine()
		local found = false

		for k,skill in pairs(rk.Skills) do
			if (skill == tradename) then
				found = true
				break
			end
		end
		if (found == false) then
 			--rk:Msg("Trade Skill \""..tostring(tradename).."\" not supported.")
			return
		end

		--rk:Msg("Trade Skill \""..tostring(tradename).."\" supported! Scanning recipes...")

		local numTradeSkills = GetNumTradeSkills()		
		if (not numTradeSkills or numTradeSkills == 0) then
			-- rk:Msg("No Tradeskills found")
			return
		end

		local skillName, skillType = GetTradeSkillInfo(1)	-- Testing to see if our first line is a header
		if skillType ~= "header" then return end			-- If not a header, then don't scan. Incomplete recipe data.
		
		RecipeKnown_GenerateDatabase()

		if (not RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][tradename]) then
			RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][tradename] = {}
		end
		if (not RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][tradename][rk.CharName]) then
			RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][tradename][rk.CharName] = {
				["data"] = {
					["current"] = tradecurrentskill,
					["max"] = trademaxskill,
				},
				["recipes"] = {},
			}
		end
		if (not RecipeKnown_CharData[rk.Realm][rk.Faction]["data"][rk.CharName]["Professions"][tradename]) then
			RecipeKnown_CharData[rk.Realm][rk.Faction]["data"][rk.CharName]["Professions"][tradename] = tradecurrentskill
		end
		local headercount = 0
		for i=1,numTradeSkills do
			skillName, skillType = GetTradeSkillInfo(i)
			if (skillType == "header" or skillType == "subheader") then
				headercount = headercount + 1
			elseif (skillType ~= "header") and (headercount > 0) then
				local tsl = GetTradeSkillRecipeLink(i)
				local tsid = tonumber(tsl:match("enchant:(%d+)"))
				-- rk:Msg("TSID:"..tostring(tsid)..", "..skillName)
				
				if (tsid) and (not RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][tradename][rk.CharName]["recipes"][tsid]) then
					RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][tradename][rk.CharName]["recipes"][tsid] = skillName
				end
			end
		end

		if (RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][tradename][rk.CharName]["data"]["current"] < tradecurrentskill) then
			RecipeKnown_CharData[rk.Realm][rk.Faction]["data"][rk.CharName]["Professions"][tradename] = tradecurrentskill
			RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][tradename][rk.CharName]["data"]["current"] = tradecurrentskill
		end
		if (RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][tradename][rk.CharName]["data"]["max"] < trademaxskill) then
			RecipeKnown_CharData[rk.Realm][rk.Faction]["recipes"][tradename][rk.CharName]["data"]["max"] = trademaxskill
		end

		RecipeKnown_UpdateWindows()
	end
	--rk:Msg("Scan Complete")
end

--== Pre-Scan Data Sorter ==--
-- Sets up the information needed for the Item Scanner to function properly.
function RecipeKnown_ScanData(ScanType)
	if (ScanType == nil) then return end
	local scaninfo = {
		["type"] = ScanType,
		["subtype"] = "none",
		["frame"] = nil,
		["updater"] = nil,
		["link"] = nil,
		["total"] = 0,
		["perpage"] = 0,
		["framepage"] = 0,
		["offset"] = 0,
		["itemlist"] = "none",
		["colorlist"] = {},
	}
	if (ScanType == "Merchant") then
		scaninfo["frame"] = MerchantFrame
		scaninfo["updater"] = MerchantFrame_UpdateMerchantInfo
		scaninfo["link"] = GetMerchantItemLink
		scaninfo["total"] = GetMerchantNumItems()
		scaninfo["perpage"] = MERCHANT_ITEMS_PER_PAGE
		scaninfo["framepage"] = MerchantFrame.page
		scaninfo["offset"] = 0
		scaninfo["itemname"] = "MerchantItem"
		scaninfo["colorlist"] = {
			"NameFrame",
			"SlotTexture",
			"ItemButtonIconTexture",
			"ItemButtonNormalTexture",
		}
	elseif (ScanType == "Auction") then
		local frame, itemname, updater, numpp, subtype, totalAuctions, offset

		if (AuctionFrameBrowse) and (AuctionFrameBrowse:IsVisible()) then
			--rk:Msg("Setting Scan Data for Auction Browsing...")
			frame = AuctionFrameBrowse
			updater = AuctionFrameBrowse_Update
			numpp = NUM_BROWSE_TO_DISPLAY
			subtype = "list"
			_, totalAuctions = GetNumAuctionItems("list")
			itemname = "BrowseButton"
			offset = FauxScrollFrame_GetOffset(BrowseScrollFrame)
		elseif (AuctionFrameBid) and (AuctionFrameBid:IsVisible()) then
			--rk:Msg("Setting Scan Data for Auction Bidding...")
			frame = AuctionFrameBid
			updater = AuctionFrameBid_Update
			numpp = NUM_BIDS_TO_DISPLAY
			subtype = "bidder"
			_, totalAuctions = GetNumAuctionItems("bidder")
			itemname = "BidButton"
			offset = FauxScrollFrame_GetOffset(BidScrollFrame)
		elseif (AuctionFrameAuctions) and (AuctionFrameAuctions:IsVisible()) then
			--rk:Msg("Setting Scan Data for My Auctions...")
			frame = AuctionFrameAuctions
			updater = AuctionFrameAuctions_Update
			numpp = NUM_AUCTIONS_TO_DISPLAY
			subtype = "owner"
			_, totalAuctions = GetNumAuctionItems("owner")
			itemname = "AuctionsButton"
			offset = FauxScrollFrame_GetOffset(AuctionsScrollFrame)
		end

		if (frame == nil) then
			--rk:Msg("Auction Frame = nil")
			return
		end

		scaninfo["subtype"] = subtype
		scaninfo["frame"] = frame
		scaninfo["updater"] = updater
		scaninfo["link"] = GetAuctionItemLink
		scaninfo["total"] = totalAuctions
		scaninfo["perpage"] = numpp
		scaninfo["framepage"] = (frame.page) + 1
		scaninfo["offset"] = offset
		scaninfo["itemname"] = itemname
		scaninfo["colorlist"] = {
			"ItemIconTexture",
		}
	elseif (ScanType == "TradePlayer") or (ScanType == "TradeTarget") then
		local itemname, link

		if (ScanType == "TradePlayer") then
			itemname = "TradePlayerItem"
			link = GetTradePlayerItemLink
		else
			itemname = "TradeRecipientItem"
			link = GetTradeTargetItemLink
		end

		scaninfo["frame"] = TradeFrame
		scaninfo["updater"] = TradeFrame_Update
		scaninfo["link"] = link
		scaninfo["total"] = MAX_TRADE_ITEMS
		scaninfo["perpage"] = MAX_TRADE_ITEMS
		scaninfo["framepage"] = 1
		scaninfo["offset"] = 0
		scaninfo["itemname"] = itemname
		scaninfo["colorlist"] = {
			"NameFrame",
			"SlotTexture",
			"ItemButtonIconTexture",
			"ItemButtonNormalTexture",
		}
	end

	if (scaninfo["frame"] == nil) or (scaninfo["type"] == "none") then
		return
	end

	return scaninfo
end

--== Item Scanner ==--
-- Scans the active items to see if any of them match the recorded databases.
function RecipeKnown_ScanItems(info)
	if (info == nil) then
		--rk:Msg("Info empty")
		return
	end
	if (not info["frame"]:IsVisible()) then
		--rk:Msg("Info[Frame] ("..tostring(info["frame"])..") not visible")
		return
	end
	local matchfound = false
	local itemlist, scanlist, colorlist, colordata = {}, {}, {}, {}

	local start = ((info["perpage"]*info["framepage"])-info["perpage"]) + 1
	local ending = start + (info["perpage"]) - 1
	if (ending > info["total"]) then ending = info["total"] end

	--rk:Msg("Scan Items Start:"..start..", Ending:"..ending)

	-- Build current Item data
	for i=start,ending do
		local i2 = i
		local itemlink, itemid

		if (info["type"] == "Auction") then
			i2 = info["offset"] + ((i-(info["framepage"]*info["perpage"]))+info["perpage"])
			itemlink = info["link"](info["subtype"],i2)
		else
			itemlink = info["link"](i2)
		end

		--rk:Msg("Itemlink:"..tostring(itemlink))
		itemid = rk:GetItemID(itemlink)

		-- Determine if it's a bad item.
		local baditem = false
		for k,badid in pairs(db.BadItems) do
			if (badid == itemid) then
				baditem = true
				break
			end
		end

		-- Ignore bad items
		if (baditem == false) then
			--rk:Msg("Item's ID:"..tostring(itemid))

			if (not itemid) or (itemid == nil) then
				--rk:Msg("ItemID = nil")
			else
				--rk:Msg("ItemID ~= nil")
				if (not scanlist[i]) then
					scanlist[i] = itemid
				end
			end
		end
	end

	--rk:Msg("Comparing Recipes...")
--[[
	local slc = 0
	for k,v in pairs(scanlist) do
		slc = slc + 1
	end
	rk:Msg("Number of Slots:"..slc)
]]

	-- Scan active slots for matching items
	for slot,itemid in pairs(scanlist) do
		--rk:Msg("Running Data for slot:"..slot)
		local iname, ilink,_,_,_,itype = GetItemInfo(itemid)
		local tsid = nil
		itemid = tonumber(itemid)

		--rk:Msg("iname:"..tostring(iname))

		if (itype == L["Itemtype Recipe"]) then
			RecipeKnown_BuildItemColorData(ilink)

			tsid = rk:Link2TsID(itemid)
			tsid = tostring(tsid)

			--rk:Msg("ItemID:"..tostring(itemid)..", TSID:"..tostring(tsid)..", IDarray:"..tostring(idarray[tsid]))

			if (idarray[tsid]) then
				--rk:Msg("ID Match Found! Recipe: "..iname)

				colorlist[slot] = itemid

				--rk:Msg("Setting data for slot "..slot)

				if (not colordata[slot]) then
					colordata[slot] = {}
				end
				if (not colordata[slot]["toons"]) then
					colordata[slot]["toons"] = idarray[tsid]["toons"]
				end
				matchfound = "Recipe"
			elseif (RecipeKnown_Tooltip_GetAlreadyKnown(ilink) == true) then
				colorlist[slot] = itemid
				if (not colordata[slot]) then
					colordata[slot] = {}
				end
				if (not colordata[slot]["toons"]) then
					colordata[slot]["toons"] = {
						[rk.CharName] = 1,
					}
				end
			end
		end
	end

	-- If Matches found, perform coloring
	if (matchfound ~= false) then
		RecipeKnown_ColorGnomes(info, colorlist, colordata, matchfound)
	end
end

function RecipeKnown_GetSingleItemKnown(link, toon)
	if (not link) or (link == nil) then return end
	local itemid = rk:GetItemID(link)
	local itemname, _,_,_,_,itemtype = GetItemInfo(link)
	if (not itemname) or (itemtype ~= L["Itemtype Recipe"]) then return end
	local tsid = rk:Link2TsID(itemid)
	local result

	if (tsid == nil) then
		return
	end

	-- rk:Msg("ItemID:"..tostring(itemid)..", TSID:"..tostring(tsid)..", IDarray:"..tostring(idarray[tsid]["toons"][toon]))

	if (idarray[tostring(tsid)]) and (idarray[tostring(tsid)]["toons"][toon]) then
		-- rk:Msg("Toon Found, Setting Results...")
		result = true
	end

	-- rk:Msg("Results:"..tostring(result).."!")

	return result
end

function rk:Link2TsID(itemid)
	if (itemid == nil) then
		return
	end

	--rk:Msg("Running Link2TsID...")

	local tsid = nil
	itemid = rk:GetItemID(itemid)
	local itemname, _,_,_,_,itemtype = GetItemInfo(itemid)
	--rk:Msg("ItemID:"..tostring(itemid)..", Itemname:"..tostring(itemname))

	if (db.ItemEnch[tonumber(itemid)]) then
		--rk:Msg("Item matched in ItemEnch List")
		tsid = db.ItemEnch[tonumber(itemid)]
	elseif (RecipeKnown_ItemID2TsID[tonumber(itemid)]) then
		--rk:Msg("Item matched in ID2TsID List")
		tsid = RecipeKnown_ItemID2TsID[tonumber(itemid)]
	else
		local item = itemname
		for k,prefix in pairs(rk.Prefix) do
			if strfind(itemname,prefix) then
				item = gsub(itemname,prefix, "")
				-- rk:Msg("Name Reducer:"..tostring(item))
				break
			end
		end

		RecipeKnown_ScanData_Build()

		-- rk:Msg("Item:"..tostring(item)..", NameArray:"..tostring(namearray[item]))

		if (namearray[item]) then
			--rk:Msg("Item matched using Namearray")
			tsid = namearray[item]
		else
			for name,id in pairs(namearray) do
				if (strmatch(name,item)) then
					--rk:Msg("Item matched using StrMatch")
					tsid = id
					break
				end
			end
		end
		if (tsid ~= nil) and (not db.ItemEnch[tonumber(itemid)]) and (not RecipeKnown_ItemID2TsID[tonumber(itemid)]) then
			--rk:Msg("Recording ItemID & TsID")
			RecipeKnown_ItemID2TsID[tonumber(itemid)] = tonumber(tsid)
		end
	end

	if tsid == nil then
		--rk:Msg("Item not matched.")
	end

	--rk:Msg("Returning TSID:"..tostring(tsid))
	return tsid
end

-- Return the Data Arrays
-- Used for other files that need the local data
function RecipeKnown_GetArrayData()
	RecipeKnown_ScanData_Build()
	return arraycount, idarray, namearray
end